package org.eGovernance.service;

import java.math.BigInteger;
import java.util.LinkedHashMap;

import org.eGovernance.constant.LookupConstants;
import org.eGovernance.model.Counters1;
import org.eGovernance.model.PanDetails;
import org.eGovernance.model.UserDetails;
import org.eGovernance.repository.Counters1Repository;
import org.eGovernance.repository.CountersRepository;
import org.eGovernance.repository.UserDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSendException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.sun.mail.smtp.SMTPAddressFailedException;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@Transactional
public class UserDetailsSVCImpl extends BaseSVCImpl implements UserDetailsSVC{

	@Autowired
	private UserDetailsRepository userDetailsRepository;
	@Autowired
	private CountersRepository cr;
	@Autowired
	private Counters1Repository cr1;
	
	

	@Override
	public Mono<UserDetails> saveUser(UserDetails userDetails) throws Exception {
		
		String password=getRandomString(10);
		try
		{
			sendMailToUser(userDetails.getFirstName(), userDetails.getUserName(), userDetails.getEmail(), password);
		}
		catch(SMTPAddressFailedException s)
		{
			System.out.println("Invalid Mail Address");
		}
		catch(MailSendException m)
		{
			System.out.println("Invalid Mail Address");
			
		}
		userDetails.set_id(getNextSequenceForUserDtls("userid"));
		userDetails.setPassword(password);
		userDetails.setActiveFlg(true);
		userDetails.setRole(LookupConstants.ROLE_USER);
	//	userDetails.setHNo("40");
		userDetails.setStatus(5001);
		
		PanDetails p=new PanDetails();
		p.setFatherName("Virender Gupta");
		Object seq = null;
		try {
			seq = getNextSequence("userid");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		p.setId(seq);
		userDetails.setPanDtls(p);
		userDetails.setStatus(5001);
		
		userDetailsRepository.save(userDetails).subscribe();
		
		return null;
	}
	
	public void sendMailToUser(String name, String userName, String mailId, String password)
	throws SMTPAddressFailedException, MailSendException
	{
		StringBuilder text=new StringBuilder();
		text.append("Dear "+name+",\n\n");
		text.append("You have been successfully registered in eGovernance. Please find below your login credentials\n\n");
		text.append("User Name : "+userName+"\n");
		text.append("Password : "+password);
		sendMail(mailId, "Registration Successful in eGovernance!!!", text.toString());
	}
	
	public Flux<UserDetails> findAll(){
		return userDetailsRepository.findAll();
	}
	
	public Mono<UserDetails> findById(String id)
	{
		return userDetailsRepository.findById(id);
	}
	
	public Mono<UserDetails> savePan(UserDetails userDtls){
		userDtls.setStatus(5001);
		return userDetailsRepository.save(userDtls);
	}

	@Override
	public Flux<UserDetails> findByStatus(int status) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Mono<UserDetails> findByUserName(String userName) {
		
		return userDetailsRepository.findByUserName(userName);
	}

	@Override
	public Mono<UserDetails> findByIdAndPassword(UserDetails u) {
		/*
		 * UserDetails user=findByUserName(u.getUserName()).block(); if(user!=null &&
		 * user.getPassword().equals(u.getPassword())) { Mono<UserDetails> userData =
		 * Mono.just(user); return userData ; }
		 */
		UserDetails user;
		DBCollection col = db.getCollection("userDetails");
		//DBObject query=BasicDBObjectBuilder.start().add("userName", u.getUserName()).add("password", u.getPassword()).get();
		DBObject query=BasicDBObjectBuilder.start().add("userName", "D").add("password", "D").get();
		DBCursor cursor = col.find(query);
		
		Counters1 c=new Counters1();
		c.setId("userid");
		c.setSeq(0);
		//cr1.insert(c).subscribe();
		//int seq=getNextSequenceForUserDtls("userid");
		
		
		while(cursor.hasNext())
		{
			user=new UserDetails();
			//cursor.next();
			user.set_id((Object) cursor.next().get("_id"));
			user.setFirstName((String) cursor.curr().get("firstName"));
			user.setLastName((String) cursor.curr().get("lastName"));
			user.setUserName((String) cursor.curr().get("userName"));
			user.setAddress1((String) cursor.curr().get("address1"));
			user.setAddress2((String) cursor.curr().get("address2"));
			user.setCity((String) cursor.curr().get("city"));
			user.setState((String) cursor.curr().get("state"));
			user.setPin((Integer) cursor.curr().get("pin"));
			user.setEmail((String) cursor.curr().get("email"));
			BigInteger mob=new BigInteger((String)cursor.curr().get("mobileNo"));
			user.setMobileNo(mob);
			user.setPassword((String) cursor.curr().get("password"));
			user.setRole((String) cursor.curr().get("role"));
			user.setActiveFlg((Boolean) cursor.curr().get("activeFlg"));
			user.setStatus((Integer) cursor.curr().get("status"));
			//user.getPanDtls().setFatherName((String) cursor.curr().get("panDtls.fatherName"));
			//PanDetails p=(PanDetails) cursor.curr().get("panDtls");
			LinkedHashMap l= (LinkedHashMap) cursor.curr().get("panDtls");
			if(l!=null)
			{
				PanDetails p=new PanDetails();
				p.setId(l.get("_id"));
				p.setName((String)l.get("name"));
				p.setDob((String)l.get("dob"));
				p.setFatherName((String)l.get("fatherName"));
				p.setPanNo((String)l.get("panNo"));
				p.setAppNo((String)l.get("appNo"));
				p.setEmail((String)l.get("email"));
				user.setPanDtls(p);
			}
			Mono<UserDetails> userData = Mono.just(user); 
			return userData ;
		}
		return null;
		
	}
	
	
}

