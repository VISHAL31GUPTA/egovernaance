package org.eGovernance.controller;

import org.eGovernance.model.PanDetails;
import org.eGovernance.model.UserDetails;
import org.eGovernance.repository.UserDetailsRepository;
import org.eGovernance.service.UserDetailsSVC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "http://localhost:4200")

public class UserDetailsController {

	@Autowired
	UserDetailsSVC userDetailsSVC;
	
	@Autowired
	UserDetailsRepository userDetailsRepository;
	
	@PostMapping("/saveUser")
	public Mono<UserDetails> saveUserDetails(@RequestBody UserDetails userd) throws Exception
	{	
		return userDetailsSVC.saveUser(userd);	
	}
	
	@GetMapping("/all")
	public Flux<UserDetails> getAll()
	{
		
		return userDetailsSVC.findAll();
	}
	
	@PostMapping("/savePAN")
	public Mono<UserDetails> saveUserDetailsAll(@RequestBody UserDetails user) throws Exception
	{	
		PanDetails p=new PanDetails();
		p.setFatherName("Virender Gupta");
		Object seq=userDetailsSVC.getNextSequence("userid");
		Mono<UserDetails> userd=userDetailsSVC.findById("5e8c64921d7bd052e6f4814e");
		UserDetails u=userd.block();
		p.setId(seq);
		u.setPanDtls(p);
		u.setStatus(5001);
		
		userDetailsSVC.savePan(u);
		
		return null;	
	}
	
	@GetMapping("/name")
	public Flux<UserDetails> findBy()
	{	
		return userDetailsRepository.findByFirstName("SAROJ");	
	}
	
	@GetMapping("/status")
	public Flux<UserDetails> findByStatus()
	{	
		return userDetailsRepository.findByStatus(5001);	
	}
	
	
	public Flux<UserDetails> findByUserName()
	{	
		return userDetailsRepository.findByStatus(5001);	
	}
	
	//@PostMapping("/searchByIdAndPass")
	@GetMapping("/searchByIdAndPass")
	//public Mono<UserDetails> searchByIdAndPass(@RequestBody UserDetails userd)
	public Mono<UserDetails> searchByIdAndPass()
	{
		
		//return userDetailsSVC.findByIdAndPassword(userd);
		return userDetailsSVC.findByIdAndPassword(new UserDetails());
	}
	
}
