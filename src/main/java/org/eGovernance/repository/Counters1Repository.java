package org.eGovernance.repository;

import org.eGovernance.model.Counters1;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Counters1Repository extends ReactiveMongoRepository<Counters1, String>{

}
