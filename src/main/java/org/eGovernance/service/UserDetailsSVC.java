package org.eGovernance.service;

import org.eGovernance.model.UserDetails;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserDetailsSVC extends BaseSVC{

	public Mono<UserDetails> saveUser(UserDetails userDetails) throws Exception;
	public Flux<UserDetails> findAll();
	public Mono<UserDetails> findById(String id);
	//savePan
	public Mono<UserDetails> savePan(UserDetails userDtls);
	public Flux<UserDetails> findByStatus(int status);
	public Mono<UserDetails> findByIdAndPassword(UserDetails u);
	public Mono<UserDetails> findByUserName(String userName);
	
}
