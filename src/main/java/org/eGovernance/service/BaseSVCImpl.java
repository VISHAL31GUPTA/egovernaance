package org.eGovernance.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

@Service
public class BaseSVCImpl implements BaseSVC{

	@Autowired
	private JavaMailSender javaMailSender;
	
	private SimpleMailMessage message=new SimpleMailMessage();
	
	MongoClient mongoClient=new MongoClient("localhost", 27017);
	DB db=mongoClient.getDB("eGovernance");
	
	@Override
	public boolean sendMail(String mailId, String sub, String text) {
		message.setTo(mailId);
		message.setSubject(sub);
		message.setText(text);
		javaMailSender.send(message);
		return true;
	}

	@Override
	public String getRandomString(int length) {
		StringBuilder sb=new StringBuilder();
		int index=0;
		String alphaNumericString="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
				+"abcdefghijklmnopqrstuvwxyz"
				+"1234567890";
		
		for(int i=0;i<length;i++)
		{
			index=(int)(Math.random()*alphaNumericString.length());
			sb.append(alphaNumericString.charAt(index));
		}
		return sb.toString();
	}
	
	public Object getNextSequence(String name) throws Exception
	{
		
		
		DBCollection collection=db.getCollection("counters");
		BasicDBObject find=new BasicDBObject();
		
		find.put("_id", name);
		BasicDBObject update=new BasicDBObject();
		update.put("$inc", new BasicDBObject("seq",1));
		DBObject obj=collection.findAndModify(find, update);
		return obj.get("seq");
	}
	
	public Object getNextSequenceForUserDtls(String name) throws Exception
	{
		DBCollection collection=db.getCollection("counters1");
		BasicDBObject find=new BasicDBObject();
		
		find.put("_id", name);
		BasicDBObject update=new BasicDBObject();
		update.put("$inc", new BasicDBObject("seq",1));
		DBObject obj=collection.findAndModify(find, update);
		return obj.get("seq");
	}

}
