package org.eGovernance.repository;

import org.eGovernance.model.Counters;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountersRepository extends ReactiveMongoRepository<Counters, String>{

}
