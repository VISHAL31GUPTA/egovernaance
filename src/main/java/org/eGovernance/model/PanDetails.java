package org.eGovernance.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document
public @Data class PanDetails {

	@Id
	private Object id;
	private String name;
	private String dob;
	private String fatherName;
	private String panNo;
	private String appNo;
	private String email;
	
}
