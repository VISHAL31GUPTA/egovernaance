package org.eGovernance.repository;

import org.eGovernance.model.UserDetails;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface UserDetailsRepository extends ReactiveMongoRepository<UserDetails, Object>{

	//@Query("{ 'name' : ?0 }")
	public Flux<UserDetails> findByFirstName(String name);
	//findByStatus
	public Flux<UserDetails> findByStatus(int status);
	public Mono<UserDetails> findByUserName(String userName);
}
