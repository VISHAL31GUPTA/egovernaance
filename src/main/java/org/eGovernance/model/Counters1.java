package org.eGovernance.model;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document
public @Data class Counters1 {

	private String id;
	private int seq;
	
}