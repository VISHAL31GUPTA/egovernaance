package org.eGovernance.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document
public @Data class Lookup {

	@Id
	private int id;
	private int lookup_value;
	private String description;
	
}
